import { Component } from '@angular/core';

@Component({
	selector: 'tidy-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'crud';
}
