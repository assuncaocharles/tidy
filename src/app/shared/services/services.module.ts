import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageService } from './message.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
	declarations: [],
	imports: [CommonModule, MatSnackBarModule],
	exports: [],
	providers: [MessageService]
})
export class ServicesModule {}
