import { API_CONFIG } from 'tidy/api/config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Costumer } from 'tidy/api/models/costumer';
@Injectable({
	providedIn: 'root'
})
export class CostumerService {
	private url = `${API_CONFIG.base_url}${API_CONFIG.version}`;

	constructor(private http: HttpClient) {}

	getCostumers(): Observable<Costumer[]> {
		return this.http.get<Costumer[]>(`${this.url}${API_CONFIG.endpoints.costumer_data}`);
	}

	saveCostumer(costumer: Costumer): Observable<Costumer> {
		return this.http.post<Costumer>(`${this.url}${API_CONFIG.endpoints.costumer_data}`, costumer);
	}

	editCostumer(id: number, costumer: Costumer): Observable<boolean> {
		return this.http.put<boolean>(`${this.url}${API_CONFIG.endpoints.costumer_data}/${id}`, costumer);
	}

	deleteCostumer(id: number): Observable<Costumer> {
		return this.http.delete<Costumer>(`${this.url}${API_CONFIG.endpoints.costumer_data}/${id}`);
	}
}
