import { TestBed } from '@angular/core/testing';

import { CostumerService } from './costumer-service.service';
import { HttpClientModule } from '@angular/common/http';

describe('CostumerService', () => {
	beforeEach(() =>
		TestBed.configureTestingModule({
			imports: [HttpClientModule]
		})
	);

	it('should be created', () => {
		const service: CostumerService = TestBed.get(CostumerService);
		expect(service).toBeTruthy();
	});
});
