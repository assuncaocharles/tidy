import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CostumerService } from './costumer-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
	declarations: [],
	imports: [CommonModule, HttpClientModule],
	exports: [],
	providers: [CostumerService]
})
export class CostumerServiceModule {}
