import { Costumer } from 'tidy/api/models/costumer';

export interface Payload {
	action: Action;
	costumer?: Costumer;
}

export enum Action {
	Edit = 'edit',
	Create = 'create'
}
