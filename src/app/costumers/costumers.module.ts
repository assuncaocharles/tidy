import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CostumerServiceModule } from './services/costumer-service/costumer-service.module';
import { CostumerService } from './services/costumer-service/costumer-service.service';
import { CostumersListComponent } from './components/costumers-list/costumers-list.component';
import {
	MatExpansionModule,
	MatButtonModule,
	MatIconModule,
	MatDialogModule,
	MatFormFieldModule,
	MatInputModule
} from '@angular/material';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ServicesModule } from 'tidy/services';
import { CostumersModalComponent } from './components/costumers-modal/costumers-modal.component';
import { ReactiveFormsModule } from '@angular/forms';

export let options: Partial<IConfig> | (() => Partial<IConfig>);

@NgModule({
	declarations: [CostumersListComponent, CostumersModalComponent],
	exports: [CostumersListComponent],
	imports: [
		CommonModule,
		CostumerServiceModule,
		MatExpansionModule,
		MatButtonModule,
		MatIconModule,
		MatSnackBarModule,
		ServicesModule,
		MatDialogModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule,
		NgxMaskModule.forRoot(options)
	],
	providers: [CostumerService],
	entryComponents: [CostumersModalComponent]
})
export class CostumersModule {}
