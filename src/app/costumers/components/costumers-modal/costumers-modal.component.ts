import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Payload, Action } from '../../models';
import { CostumerService } from 'tidy/costumers-services/costumer-service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'tidy-costumers-modal',
	templateUrl: './costumers-modal.component.html',
	styleUrls: ['./costumers-modal.component.scss']
})
export class CostumersModalComponent implements OnInit, OnDestroy {
	public costumerForm: FormGroup;
	private subscriptions: Subscription = new Subscription();

	constructor(
		public dialogRef: MatDialogRef<CostumersModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data: Payload,
		private FB: FormBuilder,
		private costumerService: CostumerService
	) {}

	ngOnInit() {
		this.costumerForm = this.FB.group({
			name: ['', Validators.required],
			email: ['', Validators.required],
			phone: ['', Validators.required],
			address: ['', Validators.required],
			city: ['', Validators.required],
			state: ['', Validators.required],
			zipcode: ['', Validators.required]
		});
		if (this.data.costumer) {
			this.costumerForm.patchValue(this.data.costumer);
		}
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	cancel(): void {
		this.dialogRef.close();
	}

	submitCostumer() {
		if (!this.costumerForm.valid) {
			return;
		}

		const costumer = this.costumerForm.value;
		switch (this.data.action) {
			case Action.Edit:
				this.subscriptions.add(
					this.costumerService.editCostumer(this.data.costumer.id, costumer).subscribe(result => {
						costumer.id = this.data.costumer.id;
						this.dialogRef.close({
							editResult: result,
							newCostumerValue: result ? costumer : this.data.costumer
						});
					})
				);
				break;
			case Action.Create:
				this.subscriptions.add(
					this.costumerService.saveCostumer(costumer).subscribe(newCostumer => {
						this.dialogRef.close({ newCostumer });
					})
				);
				break;
		}
	}
}
