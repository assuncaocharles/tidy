import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostumersModalComponent } from './costumers-modal.component';
import {
	MatButtonModule,
	MatExpansionModule,
	MatIconModule,
	MatSnackBarModule,
	MatDialogModule,
	MatFormFieldModule,
	MatInputModule,
	MAT_DIALOG_DATA
} from '@angular/material';
import { CostumerServiceModule } from 'tidy/costumers-services/costumer-service';
import { ServicesModule } from 'tidy/services';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { options } from '../../costumers.module';
import { BrowserModule } from '@angular/platform-browser';
import { MatDialogRef } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CostumersModalComponent', () => {
	let component: CostumersModalComponent;
	let fixture: ComponentFixture<CostumersModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CostumersModalComponent],
			imports: [
				BrowserModule,
				BrowserAnimationsModule,
				CostumerServiceModule,
				MatExpansionModule,
				MatButtonModule,
				MatIconModule,
				MatSnackBarModule,
				ServicesModule,
				MatDialogModule,
				ReactiveFormsModule,
				MatFormFieldModule,
				MatInputModule,
				NgxMaskModule.forRoot(options)
			],
			providers: [{ provide: MatDialogRef, useValue: {} }, { provide: MAT_DIALOG_DATA, useValue: [] }]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CostumersModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
