import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostumersListComponent } from './costumers-list.component';
import { CostumerServiceModule } from 'tidy/costumers-services/costumer-service';
import {
	MatExpansionModule,
	MatButtonModule,
	MatIconModule,
	MatSnackBarModule,
	MatDialogModule,
	MatFormFieldModule,
	MatInputModule
} from '@angular/material';
import { ServicesModule } from 'tidy/services';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { options } from '../../costumers.module';

describe('CostumersListComponent', () => {
	let component: CostumersListComponent;
	let fixture: ComponentFixture<CostumersListComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				CostumerServiceModule,
				MatExpansionModule,
				MatButtonModule,
				MatIconModule,
				MatSnackBarModule,
				ServicesModule,
				MatDialogModule,
				ReactiveFormsModule,
				MatFormFieldModule,
				MatInputModule,
				NgxMaskModule.forRoot(options)
			],
			declarations: [CostumersListComponent]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CostumersListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
