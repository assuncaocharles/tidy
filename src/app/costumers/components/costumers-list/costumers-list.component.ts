import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { CostumerService } from 'tidy/costumers-services/costumer-service';
import { Costumer } from 'tidy/api/models/costumer';
import { MessageService } from 'tidy/services';
import { MatDialog } from '@angular/material';
import { CostumersModalComponent } from '../costumers-modal/costumers-modal.component';
import { Payload, Action } from '../../models/index';

@Component({
	selector: 'tidy-costumers-list',
	templateUrl: './costumers-list.component.html',
	styleUrls: ['./costumers-list.component.scss']
})
export class CostumersListComponent implements OnInit, OnDestroy {
	private subscriptions: Subscription = new Subscription();
	public costumersList: Costumer[] = [];

	constructor(
		private cosutmerService: CostumerService,
		private messageService: MessageService,
		private dialog: MatDialog
	) {}

	ngOnInit(): void {
		this.subscriptions.add(
			this.cosutmerService.getCostumers().subscribe(costumers => {
				this.costumersList = costumers;
			})
		);
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	deleteCostumer(id: number): void {
		this.subscriptions.add(
			this.cosutmerService.deleteCostumer(id).subscribe(
				deletedCostumer => {
					this.costumersList = this.costumersList.filter(costumer => costumer.id !== deletedCostumer.id);
					this.messageService.openSnackBar('User deleted');
				},
				({ message }) => {
					this.messageService.openSnackBar(message);
				}
			)
		);
	}

	editCosutmer(costumer: Costumer): void {
		this.subscriptions.add(
			this.openDialog({ action: Action.Edit, costumer }).subscribe(result => {
				if (result) {
					const { editResult, newCostumerValue } = result;
					if (editResult) {
						this.costumersList = this.costumersList.map(currCostumer => {
							return currCostumer.id === newCostumerValue.id
								? { ...currCostumer, ...newCostumerValue }
								: currCostumer;
						});
						this.messageService.openSnackBar('Costumer edited ');
					} else {
						this.messageService.openSnackBar(
							'Some problem happening while trying to edit - Please try again'
						);
					}
				}
			})
		);
	}

	createCostumer(): void {
		this.subscriptions.add(
			this.openDialog({ action: Action.Create }).subscribe(({ newCostumer } = {}) => {
				if (newCostumer) {
					this.costumersList.push(newCostumer);
					this.messageService.openSnackBar('New costumer saved');
				}
			})
		);
	}

	openDialog(payload: Payload): Observable<any> {
		const dialogRef = this.dialog.open(CostumersModalComponent, {
			width: '800px',
			data: payload
		});
		return dialogRef.afterClosed();
	}
}
