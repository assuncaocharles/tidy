export const API_CONFIG = {
	base_url: 'https://tidy-api-test.herokuapp.com:443/api/',
	version: 'v1',
	endpoints: {
		costumer_data: '/customer_data'
	}
};
