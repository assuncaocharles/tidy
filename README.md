# Costumers CRUD

This is an crud that allows to perfom basic operations in for costumers

There's an live version that can be visulized on [https://crud-hroreetkwx.now.sh](https://crud-dygspzggcv.now.sh)

## Development server

Run `ng serve` or `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

